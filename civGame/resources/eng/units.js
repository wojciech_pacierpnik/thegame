exports.get = function get() {
  return {
    "Spearman": {
      "cost": {
        "food": "20",
        "gold": "5"
      },
      "stats": {
        "hp": "10",
        "def": "0",
        "att": "5"
      },
      "requirements": {
        "Buildings": {
          "Barracks": "1"
        }
      }
    },

    "Archer": {
      "cost": {
        "food": "30",
        "gold": "5"
      },
      "stats": {
        "hp": "10",
        "def": "0",
        "att": "15"
      },
      "requirements": {
        "Buildings": {
          "Barracks": "5"
        }
      }
    },

    "Swordsman": {
      "cost": {
        "food": "40",
        "gold": "15"
      },
      "stats": {
        "hp": "30",
        "def": "10",
        "att": "25"
      },
      "requirements": {
        "Buildings": {
          "Barracks": "10"
        }
      }
    }
  }
};
