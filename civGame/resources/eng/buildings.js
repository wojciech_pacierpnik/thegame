exports.get = function get() {
  return {
    "Farm": {
      "type": "production",
      "description": "This building produces one of basic resources - food.",
      "production": {
        "basicValue": "100",
        "type": "food",
        "increaseRatio": "1.1"
      },
      "basicCost": {
        "food": "50",
        "gold": "10",
        "increaseRatio": {
          "food": "1.25",
          "gold": "1.2"
        }
      }
    },

    "Gold Mine": {
      "type": "production",
      "description": "This building produces one of basic resources - gold.",
      "production": {
        "basicValue": "60",
        "type": "gold",
        "increaseRatio": "0.9"
      },
      "basicCost": {
        "food": "100",
        "gold": "50",
        "increaseRatio": {
          "food": "1.25",
          "gold": "1.2"
        }
      }
    },

    "Barracks": {
      "type": "military",
      "description": "This building lets you train warriors.",
      "basicCost": {
        "food": "80",
        "gold": "80",
        "increaseRatio": {
          "food": "1.25",
          "gold": "1.2"
        }
      }
    }
  }
};
