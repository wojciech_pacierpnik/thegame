exports.createRoute = function createRoute(dbConn) {
  var express = require('express');
  var router = express.Router();

  var highscoreDao = require('../../dao/HighscoreDao').createDao(dbConn);
  var logger = require('../../util/Logger')(console);

  /* GET highscore page. */
  router.get('/', function(req, res, next) {
    highscoreDao.fetchHighscore(0, 10, function(err, highscores) {
      if (err) {
        logger.log("ERROR", err);
        return res.status(500).json({ "status": "FAIL", "details": "Internal server error" });
      }
      res.json({ "status": "OK", "details": "Successfully fetched highscores", "highscores": highscores });
    });
  });

  return router;
};
