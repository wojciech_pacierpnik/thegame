exports.createRoute = function createRoute(dbConn) {
  var express = require('express');
  var router = express.Router();

  var UserGameStateDao = require('../../dao/UserGameStateDao').createDao(dbConn);
  var AdventureDao = require('../../dao/AdventureDao').createDao(dbConn);

  var logger = require('../../util/Logger')(console);
  var playerUtils = require('../../game/util/player');

  var authUtil = require('../../util/authUtil').createAuthUtil(dbConn);

  /* GET basic adventures view page. */
  router.get('/', function(req, res, next) {
    authUtil.verify(req,
      function(error, jwtTokenJson) { // onSuccess
        if (error) {
          logger.log("DEBUG", error.message);
          return res.json({ "status": "FAIL", "details": error.message });
        }

        var id = jwtTokenJson.claims.userid;

        UserGameStateDao.getUserGameState(id, function(err, userGameState) {
          if (err) {
            logger.log("WARN", err);
            return res.status(500).json({ "status": "FAIL", "details": "Internal server error" });;
          }

          AdventureDao.getAdventures(id, function(errAdv, adventuresArray) {
            if (errAdv) {
              logger.log("WARN", errAdv);
              return res.status(500).json({ "status": "FAIL", "details": "Internal server error" });
            }

            var data = {};
            data.user = {
              name: jwtTokenJson.claims.username,
              resources: playerUtils.getResourcesState(userGameState),
              buildings: playerUtils.getBuildingsState(userGameState),
              units: playerUtils.getUnitsState(userGameState),
              adventures: adventuresArray
            };

            res.json({ "status": "OK", "details": "Successfully obtained information about user's adventures", "data": data });
          });
        });
      },
      function(error) { // onFailure
        if (error.name === "JWTValidationError") {
          logger.log("WARN", error);
          return res.json({ "status": "FAIL", "details": "Missing or invalid JWT Token" });
        }
        logger.log("ERROR", error);
        res.status(500).json({ "status": "FAIL", "details": "Internal server error" });
      });
  });

  /* POST adventure's embark's decision form. */
  router.post('/embark/:adventureId', function(req, res, next) {
    authUtil.verify(req,
      function(error, jwtTokenJson) { // onSuccess
        if (error) {
          logger.log("DEBUG", error.message);
          return res.json({ "status": "FAIL", "details": error.message });
        }

        var id = jwtTokenJson.claims.userid;
        var adventureId = req.params.adventureId;

        UserGameStateDao.getUserGameState(id, function(err, userGameState) {
          if (err) {
            logger.log("WARN", err);
            return res.status(500).json({ "status": "FAIL", "details": "Internal server error" });
          }

          AdventureDao.getAdventure(adventureId, function(errAdv, adventure) {
            if (errAdv) {
              logger.log("WARN", errAdv);
              return res.status(500).json({ "status": "FAIL", "details": "Internal server error" });
            }

            var data = {};

            var unitUtils = require('../../game/util/unit');
            var battleUtils = require('../../game/util/battle');

            var units = playerUtils.getUnitsState(userGameState);
            var unitsBefore = {};

            for (var uName in units) {
              if (req.body[uName]) {
                unitsBefore[uName] = {
                  quantity: +units[uName].quantity
                };

                var requiredQuantity = +req.body[uName];
                var realQuantity = Math.min(+units[uName].quantity, requiredQuantity);
                units[uName].quantity = realQuantity;
              } else {
                unitsBefore[uName] = {
                  quantity: +units[uName].quantity
                };

                units[uName].quantity = 0;
              }
            }

            var opponents = unitUtils.getUnitsFromAdventureDescription(adventure);
            var report = battleUtils.processBattle(units, opponents);

            // TODO debug do usuniecia
            console.log("User's units state update")

            for (var repUnitName in report.user.units) {
              var diff = +report.user.units[repUnitName].quantity.before - +report.user.units[repUnitName].quantity.after;
              userGameState.units[repUnitName].quantity = unitsBefore[repUnitName].quantity - diff;

              console.log("Diff: %s; quantity before: %s; state after update: %s", diff, unitsBefore[repUnitName].quantity, userGameState.units[repUnitName].quantity);
            }

            if (report.result === "WIN") {
              var reward = adventure.details.reward;
              userGameState.resources.food += reward.food;
              userGameState.resources.gold += reward.gold;
            }

            userGameState.markModified("units");
            userGameState.save();

            if (report.result === "WIN") {
              AdventureDao.removeAdventure(adventure._id, function(err) {
                if (err) {
                  logger.log("ERROR", "Error while removing adventure with id: " + adventure._id);
                  return logger.log("ERROR", err);
                }
                logger.log("INFO", "Succefully removed adventure with id: " + adventure._id);
              });
            } else {
              for (var advUnitName in report.adventure.opponents) {
                adventure.details.opponents[advUnitName] = +report.adventure.opponents[advUnitName].quantity.after;
              }

              adventure.markModified("details");
              adventure.save();
            }

            // added reward info to report
            report.adventure.reward = adventure.details.reward;

            data.user = {
              name: jwtTokenJson.claims.username,
              resources: playerUtils.getResourcesState(userGameState),
              buildings: playerUtils.getBuildingsState(userGameState),
              units: playerUtils.getUnitsState(userGameState),
              adventure: adventure,
              report: report
            };

            res.json({ "status": "OK", "details": "Successfully obtained adventure's report", "data": data });
          });
        });
      },
      function(error) { // onFailure
        if (error.name === "JWTValidationError") {
          logger.log("WARN", error);
          return res.json({ "status": "FAIL", "details": "Missing or invalid JWT Token" });
        }
        logger.log("ERROR", error);
        res.status(500).json({ "status": "FAIL", "details": "Internal server error" });
      });
  });

  return router;
};
