exports.createRoute = function createRoute(dbConn) {
  var express = require('express');
  var router = express.Router();

  var UserGameStateDao = require('../../dao/UserGameStateDao').createDao(dbConn);
  var logger = require('../../util/Logger')(console);

  var playerUtils = require('../../game/util/player');

  var authUtil = require('../../util/authUtil').createAuthUtil(dbConn);

  /* GET basic town view page. */
  router.get('/', function(req, res, next) {
    authUtil.verify(req,
      function(error, jwtTokenJson) { // onSuccess
        if (error) {
          logger.log("DEBUG", error.message);
          return res.json({ "status": "FAIL", "details": error.message });
        }

        var id = jwtTokenJson.claims.userid;

        UserGameStateDao.getUserGameState(id, function(err, userGameState) {
          if (err) {
            logger.log("WARN", err);
            return res.status(500).json({ "status": "FAIL", "details": "Internal server error" });
          }

          var data = {};
          data.user = {
            name: jwtTokenJson.claims.username,
            resources: playerUtils.getResourcesState(userGameState),
            buildings: playerUtils.getBuildingsState(userGameState),
            units: playerUtils.getUnitsState(userGameState)
          };

          res.json({ "status": "OK", "details": "Successfully fetched information about user's barracks state", "data": data });
        });
      },
      function(error) { // onFailure
        if (error.name === "JWTValidationError") {
          logger.log("WARN", error);
          return res.json({ "status": "FAIL", "details": "Missing or invalid JWT Token" });
        }
        logger.log("ERROR", error);
        res.status(500).json({ "status": "FAIL", "details": "Internal server error" });
      });
  });

  /* TODO POST build decision form. */
  router.post('/units/build', function(req, res, next) {
    authUtil.verify(req,
      function(error, jwtTokenJson) { // onSuccess
        if (error) {
          logger.log("DEBUG", error.message);
          return res.json({ "status": "FAIL", "details": error.message });
        }

        var id = jwtTokenJson.claims.userid;

        var unitType = req.body.unitType;
        var quantity = Math.floor(+req.body.quantity);
        logger.logObj(req.body);

        UserGameStateDao.getUserGameState(id, function(err, userGameState) {
          if (err) {
            logger.log("WARN", err);
            return res.status(500).json({ "status": "FAIL", "details": "Internal server error" });
          }

          var resources = playerUtils.getResourcesState(userGameState);
          var buildings = playerUtils.getBuildingsState(userGameState);
          var units = playerUtils.getUnitsState(userGameState, buildings);

          logger.logObj(units);
          logger.log(unitType);

          if (!buildings["Barracks"] || buildings["Barracks"].level < 1) {
            res.json({ "status": "FAIL", "details": "Barracks are not built" });
          } else if (units[unitType].available && playerUtils.canAffordUnitTrain(units[unitType], quantity, resources)) {
            if (!userGameState.units[unitType]) {
              userGameState.units[unitType] = { quantity : 0 };
            }
            userGameState.units[unitType].quantity = +userGameState.units[unitType].quantity + quantity;
            userGameState.markModified("units");

            var price = playerUtils.countUnitsTotalPrice(units[unitType], quantity);

            userGameState.resources.food = +resources.food - +price.foodTotal;
            userGameState.resources.gold = +resources.gold - +price.goldTotal;

            userGameState.save();

            res.json({ "status": "OK", "details": "Successfully trained " + quantity + " " + unitType });
          } else if (!units[unitType].available) {
            res.json({ "status": "FAIL", "details": "Unit unavailable" });
          } else {
            res.json({ "status": "FAIL", "details": "Not enough resources" });
          }
        });
      },
      function(error) { // onFailure
        if (error.name === "JWTValidationError") {
          logger.log("WARN", error);
          return res.json({ "status": "FAIL", "details": "Missing or invalid JWT Token" });
        }
        logger.log("ERROR", error);
        res.status(500).json({ "status": "FAIL", "details": "Internal server error" });
      });
  });

  return router;
};
