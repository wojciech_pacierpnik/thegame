exports.createRoute = function createRoute(dbConn) {
  var express = require('express');
  var router = express.Router();

  var jwt = require('../../jwttoken/jwttoken');
  var logger = require('../../util/Logger')(console);
  var authUtil = require('../../util/authUtil').createAuthUtil(dbConn);

  var JwtToken = dbConn.JwtToken;

  /* POST user login data. */
  router.post('/login', function(req, res, next) {
    var username = req.body.username;
    var password = req.body.password;

    logger.log("DEBUG", "login rest api");
    logger.logObj("DEBUG", req.body);

    if (!username) {
      return res.json({ "status": "FAIL", "details": "Missing username" });
    }
    if (!password) {
      return res.json({ "status": "FAIL", "details": "Missing password" });
    }

    authUtil.authenticate(username, password,
      function(err, user) {
        if (err) {
          logger.log("DEBUG", err);
          return res.json({ "status": "FAIL", "details": err.message });
        }

        var jwtClaims = { username: user.username, userid: user.id, iat: +Date.now()/1000 };
        jwt.encodeJWT(jwtClaims, function(err, jwtToken) {
          // save token into db
          var jwtTokenDoc = new JwtToken({
            refUser: user.id,
            token: jwtToken
          });
          jwtTokenDoc.save(function(err, docs) {
            if (err) {
              logger.log("DEBUG", err);
            }
            res.json({ "status": "OK", "details": "Successfully logged in", "jwtToken": jwtToken });
          });
        });
      },
      function(err) {
        logger.log("ERROR", err);
        res.status(500).json({ "status": "FAIL", "details": "Something went wrong while trying to authenticate. If problem repeats, inform admin" });
      });
  });

  /* POST user logout data. */
  router.post('/logout', function(req, res, next) {
    logger.log("DEBUG", "logout rest api");
    logger.log("DEBUG", req.get("Authorization"));

    authUtil.verify(req,
      function(err, data) {
        if (err) {
          logger.log("DEBUG", err);
          return res.json({ "status": "FAIL", "details": err.message });
        }
        // invalidate token in db
        var jwtToken = authUtil.hasAuthorizationToken(req) ? authUtil.getJwtToken(req) : null;
        if (jwtToken) {
          JwtToken.findOneAndUpdate({ token: jwtToken }, { isValid: false }, function(err, docs) {
            res.json({ "status": "OK", "details": "Successfully logged out" });
          });
        }
      },
      function(err) {
        logger.log("WARN", err);
        res.json({ "status": "FAIL", "details": "Something went wrong trying to log out" });
      });
  });

  /* POST user verify token. */
  router.post('/verify', function(req, res, next) {
    logger.log("DEBUG", "verify rest api");
    logger.log("DEBUG", req.get("Authorization"));

    authUtil.verify(req,
      function(err, data) {
        if (err) {
          logger.log("DEBUG", err);
          return res.json({ "status": "FAIL", "details": err.message });
        }
        res.json({ "status": "OK", "details": "Token is valid" });
      },
      function(err) {
        logger.log("WARN", err);
        res.json({ "status": "FAIL", "details": "Something went wrong trying to verify token" });
      });
  });

  return router;
};
