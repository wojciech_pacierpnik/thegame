exports.createRoute = function createRoute(dbConn) {
  var express = require('express');
  var router = express.Router();

  var logger = require('../../util/Logger')(console);
  var UserGameStateDao = require('../../dao/UserGameStateDao').createDao(dbConn);
  var playerUtils = require('../../game/util/player');

  var authUtil = require('../../util/authUtil').createAuthUtil(dbConn);

  /* GET all info about required user's town. */
  router.get('/', function(req, res, next) {
    authUtil.verify(req,
      function(error, jwtTokenJson) { // onSuccess
        if (error) {
          logger.log("DEBUG", error.message);
          return res.json({ "status": "FAIL", "details": error.message });
        }

        var id = jwtTokenJson.claims.userid;

        UserGameStateDao.getUserGameState(id, function(err, userGameState) {
          if (err) {
            logger.log("WARN", err);
            return res.status(500).json({ "status": "FAIL", "details": "Internal server error" });
          }

          var data = {};
          data.user = {
            name: jwtTokenJson.claims.username,
            id: id,
            resources: playerUtils.getResourcesState(userGameState),
            buildings: playerUtils.getBuildingsState(userGameState)
          };

          res.json({ "status": "OK", "details": "Successfully obtained information about user's town", "data": data });
        });
      },
      function(error) { // onFailure
        if (error.name === "JWTValidationError") {
          logger.log("WARN", error);
          return res.json({ "status": "FAIL", "details": "Missing or invalid JWT Token" });
        }
        logger.log("ERROR", error);
        res.status(500).json({ "status": "FAIL", "details": "Internal server error" });
      });
  });

  /* POST build decision form. -- TODO do testow */
  router.post('/buildings/build/', function(req, res, next) {
    authUtil.verify(req,
      function(error, jwtTokenJson) { // onSuccess
        if (error) {
          logger.log("DEBUG", error.message);
          return res.json({ "status": "FAIL", "details": error.message });
        }

        var id = jwtTokenJson.claims.userid;
        var buildTargetName = req.body.buildTarget;

        if (!buildTargetName) {
          logger.log("DEBUG", "## req.body");
          logger.logObj("DEBUG", req.body);
          return res.status(500).json({ "status": "FAIL", "details": "Missing request data!" });
        }

        UserGameStateDao.getUserGameState(id, function(err, userGameState) {
          if (err) {
            logger.log("ERROR", err);
            return res.status(500).json({ "status": "FAIL", "details": "Internal server error" });
          }

          var resources = playerUtils.getResourcesState(userGameState);
          var buildings = playerUtils.getBuildingsState(userGameState);

          logger.logObj(buildings);
          logger.log(buildTargetName);

          if (playerUtils.canAffordBuild(buildings[buildTargetName], resources)) {
            if (!userGameState.buildings[buildTargetName]) {
              userGameState.buildings[buildTargetName] = { level: 0 };
            }
            userGameState.buildings[buildTargetName].level = +userGameState.buildings[buildTargetName].level + 1;
            userGameState.markModified("buildings");

            userGameState.resources.food = +resources.food - +buildings[buildTargetName].cost.food;
            userGameState.resources.gold = +resources.gold - +buildings[buildTargetName].cost.gold;

            userGameState.save(function(errSave, doc) {
              if (errSave) {
                logger.log("ERROR", errSave);
                return res.status(500).json({ "status": "FAIL", "details": "Internal server error" });
              }
              var detailsMsg = (buildTargetName + " was built on level " + userGameState.buildings[buildTargetName].level);
              res.json( {"status": "OK", "details": detailsMsg} );
            });
          } else {
            res.json( {"status": "FAIL", "details": "Not enough resources"} );
          }
        });
      },
      function(error) { // onFailure
        if (error.name === "JWTValidationError") {
          logger.log("WARN", error);
          return res.json({ "status": "FAIL", "details": "Missing or invalid JWT Token" });
        }
        logger.log("ERROR", error);
        res.status(500).json({ "status": "FAIL", "details": "Internal server error" });
      });
  });

  return router;
};
