exports.createRoute = function createRoute(dbConn) {
  var express = require('express');
  var router = express.Router();

  var User = dbConn.User;
  //var UserGameState = dbConn.UserGameState;
  var UserGameStateDao = require('../dao/UserGameStateDao').createDao(dbConn);

  /* GET account management page. */
  router.get('/', function(req, res, next) {
      var user = req.user;
      if (!user) {
        return res.redirect('/login');
      }

      res.render('account_management', { title: "Game", user: user });
  });


  /* GET activate user. */
  router.get('/activation/:id', function(req, res, next) {
    User.findOneAndUpdate({ _id: req.params.id }, { active: true }, function(err, user) {
      if (err) return next(err);

      // here user should have previous value
      // so if it hasn't been activated yet it will be false
      if (!user.active) {
        UserGameStateDao.initUserGameState(req.params.id, function(err, initUserState) {
          if (err) return next(err);
          res.render('succesfull_activation');
        });
      }
    });
  });

  return router;
};
