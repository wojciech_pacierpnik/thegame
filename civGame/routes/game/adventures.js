exports.createRoute = function createRoute(dbConn) {
  var express = require('express');
  var router = express.Router();

  var UserGameStateDao = require('../../dao/UserGameStateDao').createDao(dbConn);
  var AdventureDao = require('../../dao/AdventureDao').createDao(dbConn);

  var logger = require('../../util/Logger')(console);
  var playerUtils = require('../../game/util/player');

  /* GET basic adventures view page. */
  router.get('/', function(req, res, next) {
    if (!req.user) {
      return res.redirect('/login');
    }

    UserGameStateDao.getUserGameState(req.user.id, function(err, userGameState) {
      if (err) {
        logger.log("WARN", err);
        return next(err);
      }

      AdventureDao.getAdventures(req.user.id, function(errAdv, adventuresArray) {
        if (errAdv) {
          logger.log("WARN", errAdv);
          return next(errAdv);
        }

        var data = { title: "Game" };
        var error = req.query.err;
        if (error) {
          data.err = error;
        }

        data.user = {
          name: req.user.username,
          resources: playerUtils.getResourcesState(userGameState),
          buildings: playerUtils.getBuildingsState(userGameState),
          units: playerUtils.getUnitsState(userGameState),
          adventures: adventuresArray
        };

        res.render('game_adventures', data);
      });
    });
  });

  /* GET adventure's embark's decision form. */
  router.get('/embark/:adventureId', function(req, res, next) {
    if (!req.user) {
      return res.redirect('/login');
    }

    var adventureId = req.params.adventureId;

    UserGameStateDao.getUserGameState(req.user.id, function(err, userGameState) {
      if (err) {
        logger.log("WARN", err);
        return next(err);
      }

      AdventureDao.getAdventure(adventureId, function(errAdv, adventure) {
        if (errAdv) {
          logger.log("WARN", errAdv);
          return next(errAdv);
        }

        var data = { title: "Game" };
        var error = req.query.err;
        if (error) {
          data.err = error;
        }

        data.user = {
          name: req.user.username,
          resources: playerUtils.getResourcesState(userGameState),
          buildings: playerUtils.getBuildingsState(userGameState),
          units: playerUtils.getUnitsState(userGameState),
          adventure: adventure
        };

        res.render('game_adventures_embark', data);
      });
    });
  });

        /* TODO do testow !!! */
  /* POST adventure's embark's decision form. */
  router.post('/embark/:adventureId', function(req, res, next) {
    if (!req.user) {
      return res.redirect('/login');
    }

    var adventureId = req.params.adventureId;

    UserGameStateDao.getUserGameState(req.user.id, function(err, userGameState) {
      if (err) {
        logger.log("WARN", err);
        return next(err);
      }

      AdventureDao.getAdventure(adventureId, function(errAdv, adventure) {
        if (errAdv) {
          logger.log("WARN", errAdv);
          return next(errAdv);
        }

        var data = { title: "Game" };
        var error = req.query.err;
        if (error) {
          data.err = error;
        }

        // TODO zaimplementowac logike walki, rozstrzygania wyniku,
          //  a w razie zwyciestw przekazania nagrody i usuniecia wykonanej przygody
          var unitUtils = require('../../game/util/unit');
          var battleUtils = require('../../game/util/battle');

        var units = playerUtils.getUnitsState(userGameState);
        var unitsBefore = {};

        console.log("BEFORE PREPARATION TO BATTLE:")
        console.log("[first army]")
        console.log(units)

        for (var uName in units) {
          if (req.body[uName]) {
            unitsBefore[uName] = {
              quantity: +units[uName].quantity
            };

            var requiredQuantity = +req.body[uName];
            var realQuantity = Math.min(+units[uName].quantity, requiredQuantity);
            units[uName].quantity = realQuantity;
          } else {
            unitsBefore[uName] = {
              quantity: +units[uName].quantity
            };

            units[uName].quantity = 0;
          }
        }

        var opponents = unitUtils.getUnitsFromAdventureDescription(adventure);
        /*var opponentsBefore = {};
        for (var oName in opponents) {
          opponentsBefore[oName] = {
            quantity: +opponents[oName].quantity
          }
        }*/

        // TODO usunac debug
        console.log("BEFORE BATTLE:")
        console.log("[first army]")
        console.log(units)
        console.log("[second army]")
        console.log(opponents)

        // TODO
        var report = battleUtils.processBattle(units, opponents);

        console.log("REPORT AFTER BATTLE:");
        console.log(report);
        console.log("[units]");
        console.log(units);
        console.log("[unitsBefore]");
        console.log(unitsBefore);
        console.log("[report.user.units]");
        console.log(report.user.units);


        for (var repUnitName in report.user.units) {
          var diff = +report.user.units[repUnitName].quantity.before - +report.user.units[repUnitName].quantity.after;

          console.log("[diff]");
          console.log(diff);

          userGameState.units[repUnitName].quantity = unitsBefore[repUnitName].quantity - diff;
          //units[repUnitName].quantity = unitsBefore[repUnitName].quantity - diff;
        }

        /*console.log("[units - after recount]");
        console.log(units);*/

        if (report.result === "WIN") {
          var reward = adventure.details.reward;
          userGameState.resources.food += reward.food;
          userGameState.resources.gold += reward.gold;
        }

        userGameState.markModified("units");
        userGameState.save();

        if (report.result === "WIN") {
          AdventureDao.removeAdventure(adventure._id, function(err) {
            if (err) {
              logger.log("ERROR", "Error while removing adventure with id: " + adventure._id);
              return logger.log("ERROR", err);
            }
            logger.log("INFO", "Succefully removed adventure with id: " + adventure._id);
          });
        } else {
          for (var advUnitName in report.adventure.opponents) {
            adventure.details.opponents[advUnitName] = +report.adventure.opponents[advUnitName].quantity.after;
          }

          adventure.markModified("details");
          adventure.save();
        }

        // added reward info to report
        report.adventure.reward = adventure.details.reward;

        data.user = {
          name: req.user.username,
          resources: playerUtils.getResourcesState(userGameState),
          buildings: playerUtils.getBuildingsState(userGameState),
          units: playerUtils.getUnitsState(userGameState),
          adventure: adventure,
          report: report
        };

        res.render('game_adventures_embark_result', data);
      });
    });
  });

  return router;
};
