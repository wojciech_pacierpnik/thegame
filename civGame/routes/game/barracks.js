exports.createRoute = function createRoute(dbConn) {
  var express = require('express');
  var router = express.Router();

  var UserGameStateDao = require('../../dao/UserGameStateDao').createDao(dbConn);
  var logger = require('../../util/Logger')(console);

  var playerUtils = require('../../game/util/player');

  /* GET basic town view page. */
  router.get('/', function(req, res, next) {
    if (!req.user) {
      return res.redirect('/login');
    }

    UserGameStateDao.getUserGameState(req.user.id, function(err, userGameState) {
      if (err) {
        logger.log("WARN", err);
        return next(err);
      }

      var data = { title: "Game" };
      var err = req.query.err;
      if (err) {
        data.err = err;
      }

      data.user = {
        name: req.user.username,
        resources: playerUtils.getResourcesState(userGameState),
        buildings: playerUtils.getBuildingsState(userGameState),
        units: playerUtils.getUnitsState(userGameState)
      };

      res.render('game_barracks', data);
    });
  });

  /* POST build decision form. */
  router.post('/units/build', function(req, res, next) {
    if (!req.user) {
      return res.redirect('/login');
    }

    var unitType = req.body.unitType;
    var quantity = Math.floor(+req.body.quantity);
    logger.logObj(req.body);

    UserGameStateDao.getUserGameState(req.user.id, function(err, userGameState) {
      if (err) {
        logger.log("WARN", err);
        return next(err);
      }

      var resources = playerUtils.getResourcesState(userGameState);
      var buildings = playerUtils.getBuildingsState(userGameState);
      var units = playerUtils.getUnitsState(userGameState, buildings);

      logger.logObj(units);
      logger.log(unitType);

      if (!buildings["Barracks"] || buildings["Barracks"].level < 1) {
        res.redirect('/game/barracks?err=' + encodeURI("Barracks are not built"));
      } if (units[unitType].available && playerUtils.canAffordUnitTrain(units[unitType], quantity, resources)) {

        // TODO interakcja z baza danych powinna byc w DAO
        if (!userGameState.units[unitType]) {
          userGameState.units[unitType] = { quantity : 0 };
        }
        userGameState.units[unitType].quantity = +userGameState.units[unitType].quantity + quantity;
        userGameState.markModified("units");

        var price = playerUtils.countUnitsTotalPrice(units[unitType], quantity);

        userGameState.resources.food = +resources.food - +price.foodTotal;
        userGameState.resources.gold = +resources.gold - +price.goldTotal;

        userGameState.save();

        res.redirect('/game/barracks');
      } else if (!units[unitType].available) {
        res.redirect('/game/barracks?err=' + encodeURI("Unit unavailable"));
      } else {
        res.redirect('/game/barracks?err=' + encodeURI("Not enough resources"));
      }
    });
  });

  return router;
};
