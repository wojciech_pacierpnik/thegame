exports.createRoute = function createRoute(dbConn) {
  var express = require('express');
  var router = express.Router();

  var UserGameStateDao = require('../../dao/UserGameStateDao').createDao(dbConn);
  var logger = require('../../util/Logger')(console);

  var playerUtils = require('../../game/util/player');

  /* GET basic town view page. */
  router.get('/', function(req, res, next) {
    if (!req.user) {
      return res.redirect('/login');
    }

    UserGameStateDao.getUserGameState(req.user.id, function(err, userGameState) {
      if (err) {
        logger.log("WARN", err);
        return next(err);
      }

      var data = { title: "Game" };

      data.user = {
        name: req.user.username,
        resources: playerUtils.getResourcesState(userGameState),
        buildings: playerUtils.getBuildingsState(userGameState)
      };
      res.render('game_main_container', data);
    });
  });

  /* POST build decision form. */
  router.post('/buildings/build', function(req, res, next) {
    if (!req.user) {
      return res.redirect('/login');
    }

    var buildTargetName = req.body.buildTarget;
    logger.logObj(req.body);

    UserGameStateDao.getUserGameState(req.user.id, function(err, userGameState) {
      if (err) {
        logger.log("WARN", err);
        return next(err);
      }

      var resources = playerUtils.getResourcesState(userGameState);
      var buildings = playerUtils.getBuildingsState(userGameState);

      logger.logObj(buildings);
      logger.log(buildTargetName);

      if (playerUtils.canAffordBuild(buildings[buildTargetName], resources)) {

        // TODO interakcja z baza danych powinna byc w DAO
        if (!userGameState.buildings[buildTargetName]) {
          userGameState.buildings[buildTargetName] = { level: 0 };
        }
        userGameState.buildings[buildTargetName].level = +userGameState.buildings[buildTargetName].level + 1;
        userGameState.markModified("buildings");

        userGameState.resources.food = +resources.food - +buildings[buildTargetName].cost.food;
        userGameState.resources.gold = +resources.gold - +buildings[buildTargetName].cost.gold;

        userGameState.save();
      }

      res.redirect('/game/town');
    });
  });

  /**
    CHEATS FOR DEBUG NEEDS
  */

  /* GET additional cheated resources */
  router.get('/cheat/:resource/:value', function(req, res, next) {
    if (!req.user) {
      return res.redirect('/login');
    }

    UserGameStateDao.getUserGameState(req.user.id, function(err, userGameState) {
      if (err) {
        logger.log("WARN", err);
        return next(err);
      }

      var resource = req.params.resource;
      var value = req.params.value;

      var resources = playerUtils.getResourcesState(userGameState);

      logger.log("DEBUG", JSON.stringify(resources));
      logger.log("DEBUG", Object.keys(resources));

      if (Object.keys(resources).includes(resource)) {
        userGameState.resources[resource] = +resources[resource] + +value;
        userGameState.save();
      }

      res.redirect('/game/town');
    });
  });

  /* GET (DELETE) userGameState */
  router.get('/remove/user/game/state', function(req, res, next) {
    if (!req.user) {
      return res.redirect('/login');
    }

    UserGameStateDao.removeUserGameState(req.user.id, function(err, result) {
      if (err) {
        logger.log(err);
      }

      logger.log(result);
      res.redirect('/account');
    });
  });

  return router;
};
