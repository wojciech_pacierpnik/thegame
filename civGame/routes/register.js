exports.createRoute = function createRoute(dbConn) {
  var express = require('express');
  var router = express.Router();

  var mailer = require('../mailer/mailer');
  var User = dbConn.User;
  var sha1 = require('sha1');
  var logger = require('../util/Logger')(console);

  var APPLICATION_URL_PREFIX = "http://localhost:3000";

  /* GET registration page. */
  router.get('/', function(req, res, next) {
    if (req.user) {
      return res.redirect('/account');
    }

    var errRes = require('../resources/eng/registration_errors.json');
    var data = { title: 'The Game' };
    if (req.query.error) {
      data['error'] = errRes[req.query.error];
      if (data['error'] === undefined || data['error'] === null) {
        data['error'] = errRes['UNKNOWN'];
      }
    }
    res.render('register', data);
  });

  /* POST registration form. */
  router.post('/', function(req, res, next) {
    var login = req.body.login;
    var pass = req.body.password;
    var rPass = req.body.repeatPassword;
    var email = req.body.email;

    if (!login) {
      return res.redirect('/registration?error=1')
    }

    if (!pass) {
      return res.redirect('/registration?error=2')
    }

    if (pass !== rPass) {
      return res.redirect('/registration?error=3')
    }

    if (!email) {
      return res.redirect('/registration?error=4')
    }

    User.findOne({ username: login }, function(err, user) {
      if (user) {
        return res.redirect('/registration?error=5');
      }

      User.findOne({ email: email }, function(err, user) {
        if (user) {
          return res.redirect('/registration?error=6');
        }

        var newUser = {
          username: login,
          password: sha1(pass),
          email: email
        };
        User.create(newUser, function(err, user) {
          if (err) return next(err);

          var activationLink = APPLICATION_URL_PREFIX + "/account/activation/" + user._id;

          var mailOptions = {
            from: 'Game <web20@t.pl>',
            to: user.email,
            subject: '[GAME] Account activation',
            html: '<p>Successful registration. In order to activate account, go to: '
              + '<a href="' + activationLink + '">'
              + activationLink + '</a></p>'
          };

          mailer.createTransport().sendMail(mailOptions, function (err, info) {
            if (err) {
              logger.log(err);
              return next(err);
            }
            res.render('succesfull_register', { title: "Game" });
          });
        });
      });
    });
  });

  return router;
}
