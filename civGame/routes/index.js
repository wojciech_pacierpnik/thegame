exports.createRoute = function createRoute(dbConn) {
  var express = require('express');
  var router = express.Router();

  /* GET home page. */
  router.get('/', function(req, res, next) {
    if (req.user) {
      return res.redirect('/account');
    }

    res.render('index', { title: 'The Game' });
  });

  return router;
};
