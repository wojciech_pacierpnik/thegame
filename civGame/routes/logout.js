exports.createRoute = function createRoute(dbConn) {
  var express = require('express');
  var router = express.Router();

  var logger = require('../util/Logger')(console);

  /* GET logout page. */
  router.get('/', function(req, res, next) {
    if (!req.user) {
      return res.redirect('/');
    }

    req.session.destroy(function(err) {
      if (err) {
        logger.log("ERROR", err);
      }
      res.redirect('/');
    });
  });

  return router;
};
