exports.createRoute = function createRoute(dbConn) {
  var express = require('express');
  var router = express.Router();

  var passport = require('passport');

  /* GET login page. */
  router.get('/', function(req, res, next) {
    if (req.user) {
      return res.redirect('/account');
    }

    var errRes = require('../resources/eng/login_errors.json');
    var data = { title: 'The Game' };
    var error = req.query.error;
    if (error) {
      if (typeof error === "string") {
        data['error'] = decodeURI(error);
      } else {
        data['error'] = errRes[error];
        if (data['error'] === undefined || data['error'] === null) {
          data['error'] = errRes['UNKNOWN'];
        }
      }
    }
    res.render('login', data);
  });

  /* POST user login data. */
  router.post('/', function(req, res, next) {
    passport.authenticate('local', function(err, user, info) {
      if (err) {
        return next(err);
      }

      if (!user) {
        return res.redirect('/login?error=' + encodeURI(info.message));
      }

      req.logIn(user, function(err) {
        if (err) {
          return next(err);
        }

        return res.redirect('/game/town');
      });
    })(req, res, next);
  });

  return router;
};
