var dateFormat = require('dateformat');

module.exports = function Logger(output) {
  if (!(this instanceof Logger)) {
    return new Logger(output);
  }

  this.log = function log(level, message) {
    if (arguments.length === 1) {
      message = level;
      level = "INFO";
    }
    output.log(dateFormat(new Date(), 'yyyy/mm/dd HH:MM:ss') + "\t[" + level + "] " + message);
  };

  this.logObj = function logObj(level, obj) {
    var message = obj;
    if (arguments.length === 1) {
      message = level;  // in this case there is object 'obj' in parameter 'level'
      level = "INFO";
    }
    message = JSON.stringify(message);
    this.log(level, message);
  };
};
