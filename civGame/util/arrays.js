module.exports = (function() {
  var swap = function swap(array, sourceIndex, targetIndex) {
    array[targetIndex] = [array[sourceIndex], array[sourceIndex] = array[targetIndex]][0];
  };

  var shuffle = function shuffle(array) {
    var index = array.length, randomIndex;
    while (index !== 0) {
      randomIndex = Math.floor(Math.random() * index);
      --index;
      swap(array, index, randomIndex);
    }
    return array;
  };

  var min = function min(array) {
    return Math.min.apply(null, array);
  };

  var max = function max(array) {
    return Math.max.apply(null, array);
  };

  return {
    "swap": swap,
    "shuffle": shuffle,
    "min": min,
    "max": max
  };
})();
