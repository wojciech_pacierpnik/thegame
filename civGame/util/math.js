module.exports = (function() {
  var draw = function draw(chance) {
    // return true if chance succedded and false otherwise
    return Math.random() < chance;
  };

  var randomDivisionOfOne = function randomDivisionOfOne(parts) {
    var rest = 1;
    var result = [];
    for (var i = 0, n = (parts-1); i < n; ++i) {
      var rand = Math.random() * rest;
      rest -= rand;
      result.push(rand);
    }
    result.push(rest);
    return result;
  };

  return {
    "draw": draw,
    "SUCCESSFUL_DRAW": true,
    "FAILED_DRAW": false,

    "randomDivisionOfOne": randomDivisionOfOne
  };
})();
