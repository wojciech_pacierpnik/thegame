exports.createAuthUtil = function createAuthUtil(dbConn) {
  var sha1 = require('sha1');

  var jwt = require('../jwttoken/jwttoken');
  var logger = require('./Logger')(console);

  var User = dbConn.User;
  var JwtToken = dbConn.JwtToken;

  var authenticate = function authenticate(username, pass, onSuccess, onFailure) {
    pass = sha1(pass);
    User.findOne({ 'username': username }, function(err, user) {
      if (err) {
        return onFailure(err);
      }

      if (!user) {
        return onSuccess(new Error("User doesn't exist"));
      }

      if (user.password === pass) {
        onSuccess(null, user);
      } else {
        onSuccess(new Error("Invalid password"));
      }
    });
  };

  var hasAuthorizationToken = function hasAuthorizationToken(req) {
    var authData = req.get("Authorization");
    if (authData) {
      return (authData.substring(0,6) === "Bearer");
    }
    return false;
  };

  var getJwtToken = function getJwtToken(req) {
    var authData = req.get("Authorization");
    return authData.substring(7);
  };

  var verify = function verify(req, onSuccess, onFailure) {
    if (hasAuthorizationToken(req)) {
      var jwtData = getJwtToken(req);
      jwt.verifyJWT(jwtData, function(err, data) {
        if (err) {
          return onFailure(err);
        }
        // verify token in db if is valid
        JwtToken.findOne({ token: jwtData }, function(err, foundJwtToken) {
          if (err) {
            logger.log("WARN", err);
            return onFailure(err);
          }

          if (!foundJwtToken) {  // not found in db
            return onSuccess(new Error("Token doesn't exists in database"));
          }

          if (!foundJwtToken.isValid) {  // invalidated
            return onSuccess(new Error("Token is invalid"));
          }

          onSuccess(null, data);
        });
      });
    } else {
      onSuccess(new Error("Missing authorization token"));
    }
  };

  return {
    'authenticate': authenticate,
    'hasAuthorizationToken': hasAuthorizationToken,
    'getJwtToken': getJwtToken,
    'verify': verify
  };
};
