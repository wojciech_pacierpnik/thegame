var nodemailer = require('nodemailer');

exports.createTransport = function createTransport() {
  var selfSignedConfig = {
    host: 't.pl',
    port: 465,
    secure: true, // uzywa TLS
    auth: {
      user: 'web20@t.pl', pass: 'stud234'
    },
    tls: {
      // nie przerywa przy blednym certyfikacie
      rejectUnauthorized: false
    }
  };

  return nodemailer.createTransport(selfSignedConfig);
};
