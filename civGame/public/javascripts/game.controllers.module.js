(function() {
  var module = angular.module('game-controllers', ['game-services']);

  module.controller('NavController', ['navService', function(navService) {
    this.selectTown = function selectTown() {
      navService.selectTown();
    };

    this.selectBarracks = function selectBarracks() {
      navService.selectBarracks();
    };

    this.selectAdventures = function selectAdventures() {
      navService.selectAdventures();
    };

    this.selectHighscores = function selectHighscores() {
      navService.selectHighscores();
    };
  }]);

  module.controller('TownController', ['$scope', 'gameResourcesService', 'townDataService', function($scope, gameResourcesService, townDataService) {
    var that = this;
    this.townDataLoadedAt = null;
    this.town = null;

    this.getTown = function getTown() {
      townDataService.getTown(function(err, responseData) {
        that.townDataLoadedAt = Date.now();
        that.town = responseData.data;
        gameResourcesService.refreshGameResourcesState(that.town.user.resources);
      });
    }

    this.build = function build(targetBuildingName) {
      townDataService.build(targetBuildingName, function() {
        that.getTown();
      });
    }

    $scope.$on('app:game:town:refresh', function() {
      that.getTown();
    });

  }]);

  module.controller('BarracksController', ['$scope', 'gameResourcesService', 'barracksService', function($scope, gameResourcesService, barracksService) {
    var that = this;
    this.barracksDataLoadedAt = null;
    this.barracks = null;

    this.unitsToTrain = {};

    this.getBarracks = function getBarracks() {
      barracksService.getBarracks(function(err, responseData) {
        that.barracksDataLoadedAt = Date.now();
        that.barracks = responseData.data;

        gameResourcesService.refreshGameResourcesState(that.barracks.user.resources);
      });
    };

    this.trainUnit = function trainUnit(unitName) {
      barracksService.trainUnit({ unitType: unitName, quantity: that.unitsToTrain[unitName].amountToTrain }, function() {
        that.getBarracks();
        that.unitsToTrain[unitName].amountToTrain = 0;  // reset form for trained units
      });
    };

    $scope.$on('app:game:barracks:refresh', function() {
      that.getBarracks();
    });

  }]);

  module.controller('AdventuresController', ['$scope', 'gameResourcesService', 'adventuresService', function($scope, gameResourcesService, adventuresService) {
    var that = this;

    this.adventuresDataLoadedAt = null;
    this.adventures = null;
    this.selectedAdventure = null;
    this.unitsToSendOnAdventure = {};
    this.adventureReport = null;

    this.getAdventures = function getAdventures(denyFullReset) {
      adventuresService.getAdventures(function(err, responseData) {
        if (!denyFullReset) {
          that.resetAdventures();
        } else {
          that.resetAdventureSelection();
        }
        that.adventuresDataLoadedAt = Date.now();
        that.adventures = responseData.data;

        gameResourcesService.refreshGameResourcesState(that.adventures.user.resources);
      });
    };

    this.selectAdventure = function selectAdventure(adventure) {
      that.selectedAdventure = adventure;
    };

    this.resetUnitsToSendOnAdventureState = function resetUnitsToSendOnAdventureState() {
      for (var unit in that.unitsToSendOnAdventure) {
        that.unitsToSendOnAdventure[unit] = 0;
      }
    };

    this.resetAdventureSelection = function resetAdventureSelection() {
      that.selectAdventure(null);
      that.resetUnitsToSendOnAdventureState();
    };

    this.resetAdventures = function resetAdventures() {
      that.resetAdventureSelection();
      that.adventureReport = null;
    };

    this.sendOnAdventure = function sendOnAdventure(id) {
      adventuresService.sendOnAdventure(id, that.unitsToSendOnAdventure, function(err, responseData) {
        that.getAdventures(true);
        that.adventureReport = responseData.data.user.report;
      });
    };

    $scope.$on('app:game:adventures:refresh', function() {
      that.getAdventures();
    });

  }]);

  module.controller('HighscoreController', ['$scope', 'highscoresService', function($scope, highscoresService) {
    var that = this;
    this.highscores = null;

    this.getHighscores = function getHighscores() {
      highscoresService.getHighscores(function(err, responseData) {
        that.highscores = responseData.highscores;
      });
    };

    $scope.$on('app:game:highscores:refresh', function() {
      that.getHighscores();
    });

  }]);
})();
