(function(){
  var module = angular.module('game', ['game-services', 'game-controllers', 'game-directives']);

  module.controller('MainController',
    ['$scope', 'authService', 'gameResourcesService', 'navService',
    function($scope, authService, gameResourcesService, navService) {

    //input
    this.username = "";
    this.password = "";

    //output
    this.jwtToken = "";

    //general response field
    this.response = {
      status: "",
      details: ""
    };

    //navigation control field
    this.activeView = 1;

    //self-reference for use in functions body
    var that = this;

    //error-handling logic
    this.errors = [];

    this.clearErrors = function clearErrors() {
      that.errors = [];
    };

    $scope.$on('app:error', function(event, err) { that.errors.push(err); });

    //common fields and logic
    this.gameResources = null;

    this.refreshGameResourcesState = function refreshGameResourcesState(newResourcesState) {
      gameResourcesService.refreshGameResourcesState(newResourcesState);
    };

    $scope.$on('app:game:res:refresh', function(event, gameResources) { that.gameResources = gameResources; });

    // NAVIGATION LOGIC

    this.setActiveView = function setActiveView(nr) {
      that.activeView = +nr;
    };

    this.isActive = function isActive(nr) {
      return +that.activeView === nr;
    };

    // NAVIGATION LOGIC -- END

    // AUTHORIZATION LOGIC

    this.login = function login() {
      authService.login({ username: that.username, password: that.password }, function(err, jwtToken) {
        that.jwtToken = jwtToken;
        navService.selectTown();
      });
    };

    this.logout = function logout() {
      authService.logout();
      that.jwtToken = "";
    };

    // AUTHORIZATION LOGIC -- END

  }]);

})();
