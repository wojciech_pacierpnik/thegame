(function() {
  var module = angular.module('game-services', []);

  module.factory('errorsService', ['$rootScope', function($rootScope) {
    var checkForErrors = function checkForErrors(responseData) {
      if (responseData.data && (responseData.data.status === "FAIL")) {
        responseData.data.occurrenceDate = Date.now();
        $rootScope.$broadcast('app:error', responseData.data);
      } else if (responseData.status === 500) {
        if (responseData.data.details) {
          responseData.data.occurrenceDate = Date.now();
          $rootScope.$broadcast('app:error', responseData.data);
        } else {
          $rootScope.$broadcast('app:error', { "details": responseData.statusText, "occurrenceDate": Date.now() } );
        }
      } else if (responseData.status < 200 || responseData.status > 399) {
        $rootScope.$broadcast('app:error', { "details": responseData.statusText, "occurrenceDate": Date.now() } );
      }
    };

    return {
      'checkForErrors': checkForErrors
    };
  }]);

  module.factory('authService', ['$http', 'errorsService', function($http, errorsService) {
    var jwtToken = "";

    //credentialsObject = { username: that.username, password: that.password }
    var login = function login(credentialsObject, onLoginSuccess) {
      $http.post("/api/io/login", credentialsObject)
        .then(function(response) {
          console.log(response.data);

          jwtToken = response.data.jwtToken;
          errorsService.checkForErrors(response);
          if (response.data.status === "OK") {
            if (typeof onLoginSuccess === "function") {
              onLoginSuccess(null, jwtToken);
            }
          }
        }, function(errorRes) {
          errorsService.checkForErrors(errorRes);
          console.log(errorRes);
        });
    };

    var logout = function logout() {
      $http.post("/api/io/logout",
        {},
        {
          headers: {
            "Authorization": "Bearer " + jwtToken
          }
        })
        .then(function(response) {
          console.log(response.data);

          jwtToken = "";
          errorsService.checkForErrors(response);
        }, function(errorRes) {
          errorsService.checkForErrors(errorRes);
          console.log(errorRes);
        });
    };

    var getJwtToken = function getJwtToken() {
      return jwtToken;
    };

    return {
      'login': login,
      'logout': logout,
      'getJwtToken': getJwtToken
    };
  }]);

  module.factory('gameResourcesService', ['$rootScope', function($rootScope) {
    var gameResources = null;

    var getGameResources = function getGameResources() {
      return gameResources;
    };

    var refreshGameResourcesState = function refreshGameResourcesState(newResourcesState) {
      gameResources = newResourcesState;
      $rootScope.$broadcast('app:game:res:refresh', gameResources);
    };

    return {
      'getGameResources': getGameResources,
      'refreshGameResourcesState': refreshGameResourcesState
    };
  }]);

  module.factory('townDataService', ['$http', 'errorsService', 'authService', function($http, errorsService, authService) {
    var getTown = function getTown(onSuccess) {
      $http.get("/api/game/town",
        {
          headers: {
            "Authorization": "Bearer " + authService.getJwtToken()
          }
        })
        .then(function(response) {
          console.log(response.data);

          errorsService.checkForErrors(response);

          if (typeof onSuccess === 'function') {
            onSuccess(null, response.data);
          }
        }, function(errorRes) {
          errorsService.checkForErrors(errorRes);
          console.log(errorRes);
        });
    }

    var build = function build(targetBuildingName, onSuccess) {
      $http.post("/api/game/town/buildings/build/",
        {
          buildTarget: targetBuildingName
        },
        {
          headers: {
            "Authorization": "Bearer " + authService.getJwtToken()
          }
        })
        .then(function(response) {
          console.log(response.data);

          errorsService.checkForErrors(response);

          if (typeof onSuccess === 'function') {
            onSuccess(null, response.data);
          }
        }, function(errorRes) {
          errorsService.checkForErrors(errorRes);
          console.log(errorRes);
        });
    }

    return {
      'getTown': getTown,
      'build': build
    };
  }]);

  module.factory('barracksService', ['$http', 'errorsService', 'authService', function($http, errorsService, authService) {
    var getBarracks = function getBarracks(onSuccess) {
      $http.get("/api/game/barracks",
        {
          headers: {
            "Authorization": "Bearer " + authService.getJwtToken()
          }
        })
        .then(function(response) {
          console.log(response.data);

          errorsService.checkForErrors(response);

          if (typeof onSuccess === 'function') {
            onSuccess(null, response.data);
          }
        }, function(errorRes) {
          errorsService.checkForErrors(errorRes);
          console.log(errorRes);
        });
    };

    var trainUnit = function trainUnit(unitTrainObject, onSuccess) {
      console.log("[Unit name: %s][Amount to train: %s]", unitTrainObject.unitName, unitTrainObject.quantity);

      //unitTrainObject = { unitType: unitName, quantity: that.unitsToTrain[unitName].amountToTrain }
      $http.post("/api/game/barracks/units/build",
        unitTrainObject,
        {
          headers: {
            "Authorization": "Bearer " + authService.getJwtToken()
          }
        })
        .then(function(response) {
          console.log(response.data);

          errorsService.checkForErrors(response);

          if (typeof onSuccess === 'function') {
            onSuccess(null, {});
          }
        }, function(errorRes) {
          errorsService.checkForErrors(errorRes);
          console.log(errorRes);
        });
    };

    return {
      'getBarracks': getBarracks,
      'trainUnit': trainUnit
    };
  }]);

  module.factory('adventuresService', ['$http', 'errorsService', 'authService', function($http, errorsService, authService) {
    var getAdventures = function getAdventures(onSuccess) {
      $http.get("/api/game/adventures",
        {
          headers: {
            "Authorization": "Bearer " + authService.getJwtToken()
          }
        })
        .then(function(response) {
          console.log(response.data);

          errorsService.checkForErrors(response);

          if (typeof onSuccess === 'function') {
            onSuccess(null, response.data);
          }
        }, function(errorRes) {
          errorsService.checkForErrors(errorRes);
          console.log(errorRes);
        });
    };

    var sendOnAdventure = function sendOnAdventure(id, unitsToSendOnAdventure, onSuccess) {
      console.log("Send on adventure with id: %s", id);

      $http.post("/api/game/adventures/embark/" + id,
        unitsToSendOnAdventure,
        {
          headers: {
            "Authorization": "Bearer " + authService.getJwtToken()
          }
        })
        .then(function(response) {
          console.log(response.data);

          errorsService.checkForErrors(response);

          if (typeof onSuccess === 'function') {
            onSuccess(null, response.data);
          }
        }, function(errorRes) {
          errorsService.checkForErrors(errorRes);
          console.log(errorRes);
        });
    };

    return {
      'getAdventures': getAdventures,
      'sendOnAdventure': sendOnAdventure
    };
  }]);

  module.factory('highscoresService', ['$http', 'errorsService', 'authService', function($http, errorsService, authService) {
    var getHighscores = function getHighscores(onSuccess) {
      $http.get("/api/game/highscore",
        {
          headers: {
            "Authorization": "Bearer " + authService.getJwtToken()
          }
        })
        .then(function(response) {
          console.log(response.data);

          errorsService.checkForErrors(response);

          if (typeof onSuccess === 'function') {
            onSuccess(null, response.data);
          }
        }, function(errorRes) {
          errorsService.checkForErrors(errorRes);
          console.log(errorRes);
        });
    };

    return {
      'getHighscores': getHighscores
    };
  }]);

  module.factory('navService', ['$rootScope', function($rootScope) {
    var selectTown = function selectTown() {
      $rootScope.$broadcast('app:game:town:refresh', {});
    };

    var selectBarracks = function selectBarracks() {
      $rootScope.$broadcast('app:game:barracks:refresh', {});
    };

    var selectAdventures = function selectAdventures() {
      $rootScope.$broadcast('app:game:adventures:refresh', {});
    };

    var selectHighscores = function selectHighscores() {
      $rootScope.$broadcast('app:game:highscores:refresh', {});
    };

    return {
      'selectTown': selectTown,
      'selectBarracks': selectBarracks,
      'selectAdventures': selectAdventures,
      'selectHighscores': selectHighscores
    };
  }]);
})();
