(function() {
  var module = angular.module('game-directives', []);

  module.directive('loginModule', function() {
    return {
      restrict: "E",
      templateUrl: "/views/templates/login_module_template.html"
    };
  });

  module.directive('navModule', function() {
    return {
      restrict: "E",
      templateUrl: "/views/templates/nav_module_template.html"
    };
  });

  module.directive('resourcesModule', function() {
    return {
      restrict: "E",
      templateUrl: "/views/templates/resources_module_template.html"
    };
  });

  module.directive('townModule', function() {
    return {
      restrict: "E",
      templateUrl: "/views/templates/town_module_template.html"
    };
  });

  module.directive('barracksModule', function() {
    return {
      restrict: "E",
      templateUrl: "/views/templates/barracks_module_template.html"
    };
  });

  module.directive('adventuresModule', function() {
    return {
      restrict: "E",
      templateUrl: "/views/templates/adventures_module_template.html"
    };
  });

  module.directive('adventuresListView', function() {
    return {
      restrict: "E",
      templateUrl: "/views/templates/adventures_list_view_template.html"
    };
  });

  module.directive('adventuresDetailsView', function() {
    return {
      restrict: "E",
      templateUrl: "/views/templates/adventures_details_view_template.html"
    };
  });

  module.directive('adventuresReportView', function() {
    return {
      restrict: "E",
      templateUrl: "/views/templates/adventures_report_view_template.html"
    };
  });

  module.directive('highscoresModule', function() {
    return {
      restrict: "E",
      templateUrl: "/views/templates/highscores_module_template.html"
    };
  });

  module.directive('errorConsole', function() {
    return {
      restrict: "E",
      templateUrl: "/views/templates/error_console_template.html"
    };
  });

})();
