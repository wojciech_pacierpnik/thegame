var JWT = require('jwt-async');

module.exports = (function() {
  var conf = require('./conf.json');

  function getJwt() {
    var jwt = new JWT();
    jwt.setSecret(conf.secretKey);
    return jwt;
  }

  return {
    encodeJWT: function encodeJWT(jwtClaims, callback) {
      var jwt = getJwt();
      jwt.sign(jwtClaims, callback);
    },

    verifyJWT: function verifyJWT(data, callback) {
      var jwt = getJwt();
      jwt.verify(data, callback);
    }
  }
})();
