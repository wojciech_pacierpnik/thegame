exports.createConfiguration = function createConfiguration(dbConn) {
  var passport = require('passport'),
      LocalStrategy = require('passport-local').Strategy;

  var User = dbConn.User;
  var sha1 = require('sha1');

  var logger = require('../util/Logger')(console);

  passport.use(new LocalStrategy({
      usernameField: 'login'
    },
    function(username, password, done) {
      User.findOne({ username: username }, function(err, user) {
        if (err) {
          logger.log("ERROR", err);
          return done(err);
        }

        if (!user) {
          return done(null, false, { message: 'Incorrect username.' });
        }

        if (sha1(password) !== user.password) {
          return done(null, false, { message: 'Incorrect password.' });
        }

        if (!user.active) {
          return done(null, false, { message: 'Account inactive, check your activation email.' });
        }

        return done(null, user);
    });
  }));

  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user);
    });
  });

  return {
    "configuredPassport": passport
  };
};
