module.exports = (function() {
  var getBuildingsState = function getBuildingsState(userGameState) {
    var buildings = require('../../resources/eng/buildings').get();
    var buildUtils = require('./build');

    var userBuildings = userGameState.buildings;
    for (var key in buildings) {
      if (userBuildings[key]) {
        buildings[key].level = userBuildings[key].level;
      } else {
        buildings[key].level = 0;
      }

      buildings[key].cost = buildUtils.evaluateNextLevelCost(buildings[key].basicCost, buildings[key].level);

      if (buildings[key].production) {
        buildings[key].production.currentValue = buildUtils.evaluateProduction(buildings[key].production, buildings[key].level);
        buildings[key].production.nextValue = buildUtils.evaluateNextLevelProduction(buildings[key].production, buildings[key].level);
      }
    }

    return buildings;
  };

  var getResourcesState = function getResourcesState(userGameState) {
    return { food: Math.floor(+userGameState.resources.food), gold: Math.floor(+userGameState.resources.gold) };
  };

  var canAffordBuild = function canAffordBuild(building, resources) {
    if ((building.cost.food <= resources.food)
        && (building.cost.gold <= resources.gold)) {
        return true;
      }
    return false;
  };

  var getUnitsState = function getUnitsState(userGameState, buildings) {
    var units = require('../../resources/eng/units').get();
    if (!buildings) {
      buildings = getBuildingsState(userGameState);
    }

    var userUnits = userGameState.units;
    for (var key in units) {
      if (userUnits[key]) {
        units[key].quantity = userUnits[key].quantity;
      } else {
        units[key].quantity = 0;
      }

      units[key].available = true;
      for (var bReqKey in units[key].requirements.Buildings) {
        if (+buildings[bReqKey].level < +units[key].requirements.Buildings[bReqKey]) {
          units[key].available = false;
          break;
        }
      }
    }

    return units;
  };

  var canAffordUnitTrain = function canAffordUnitTrain(unit, quantity, resources) {
    var price = countUnitsTotalPrice(unit, quantity);
    if ((price.foodTotal <= resources.food) && (price.goldTotal <= resources.gold)) {
      return true;
    }
    return false;
  };

  var countUnitsTotalPrice = function countUnitsTotalPrice(unit, quantity) {
    var foodTotal = (+unit.cost.food) * (+quantity);
    var goldTotal = (+unit.cost.gold) * (+quantity);
    return { "foodTotal": foodTotal, "goldTotal": goldTotal };
  }

  return {
    "getBuildingsState": getBuildingsState,
    "getResourcesState": getResourcesState,
    "canAffordBuild": canAffordBuild,
    "getUnitsState": getUnitsState,
    "canAffordUnitTrain": canAffordUnitTrain,
    "countUnitsTotalPrice": countUnitsTotalPrice
  };
})();
