var logger = require('../../util/Logger')(console);

module.exports = (function() {

  var calculateHighscore = function calculateHighscore(userGameState) {
    var RESOURCE_FACTOR = 0.0001;
    var BUILDINGS_FACTOR = 1.5;
    var UNITS_FACTOR = 5.0;

    var resources = userGameState.resources;
    var buildings = userGameState.buildings;
    var units = userGameState.units;

    var total = 0;

    //resources
    total += (+resources.food * RESOURCE_FACTOR) + (+resources.gold * RESOURCE_FACTOR);

    //buildings
    for (var bName in buildings) {
      total += (+buildings[bName].level * BUILDINGS_FACTOR);
    }

    //units
    for (var uName in units) {
      total += (+units[uName].quantity * BUILDINGS_FACTOR);
    }

    return Math.ceil(total);
  };

  var mergeUserGameStatesWithUsernames = function mergeUserGameStatesWithUsernames(userGameStates, users) {
    var newUserGameStates = [];
    for (var i = 0; i < userGameStates.length; ++i) {
      var ugs = userGameStates[i].toObject();   // It MUST be done like that, because objects returned by mongoose are frozen
      for (var j = 0; j < users.length; ++j) {
        if (""+userGameStates[i].refUser === ""+users[j]._id) {
          ugs.username = users[j].username;
          break;
        }
      }
      newUserGameStates.push(ugs);
    }
    return newUserGameStates;
  };

/*
  // mapa nie zachowa kolejnosci elementow a tutaj sortowanie jest kluczowe (!)
  var mergeUserGameStatesWithUsernames = function mergeUserGameStatesWithUsernames(userGameStates, users) {
    var merged = {};

    for (var i = 0; i < userGameStates.length; ++i) {
      merged[userGameStates[i].refUser] = userGameStates[i];
    }

    for (var i = 0; i < users.length; ++i) {
      merged[users[i]._id].username = users[i].username;
    }

    return merged;
  };
*/

  return {
    "calculateHighscore": calculateHighscore,
    "mergeUserGameStatesWithUsernames": mergeUserGameStatesWithUsernames
  };
})();
