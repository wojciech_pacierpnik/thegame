module.exports = (function() {

  var valueExists = function valueExists(obj) {
    return ((obj !== undefined) && (obj !== null));
  };

  var getUnitsFromAdventureDescription = function getUnitsFromAdventureDescription(adventure) {
    var units = require('../../resources/eng/units').get();
    var opponents = adventure.details.opponents;

    //console.log("getUnitsFromAdventureDescription: ")
    //console.log(units)

    for (var key in units) {
      if (valueExists(opponents[key])) {
        units[key].quantity = +opponents[key];
      } else {
        units[key].quantity = 0;
      }
    }

    return units;
  };

  /*var mergeChangesToAdventure = function mergeChangesToAdventure(adventure, opponents) {
    for (var key in opponents) {
      adventure.details.opponents[key] = +opponents[key].quantity;
    }
  };*/

  return {
    "getUnitsFromAdventureDescription": getUnitsFromAdventureDescription
  };
})();
