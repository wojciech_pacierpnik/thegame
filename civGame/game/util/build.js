module.exports = (function() {
  var evaluateNextLevelCost = function evaluateNextLevelCost(basicCost, currLevel) {
    var food = +basicCost.food;
    var gold = +basicCost.gold;
    var foodIncreaseRatio = +basicCost.increaseRatio.food;
    var goldIncreaseRatio = +basicCost.increaseRatio.gold;
    var lvl = +currLevel;

    var logger = require('../../util/Logger')(console);
    logger.log("DEBUG", "evaluateNextLevelCost(): [food=" + food + "; gold=" + gold + "; level=" + lvl + "]");

    return {
      food: Math.ceil(food + (food * lvl * foodIncreaseRatio)),
      gold: Math.ceil(gold + (gold * lvl * goldIncreaseRatio))
    }
  }

  var evaluateProduction = function evaluateProduction(production, currLevel) {
    var val = +production.basicValue;
    var ratio = +production.increaseRatio;
    var lvl = +currLevel;

    if (production.type === "food") {
      return Math.ceil(val + (val * lvl * ratio));
    } else if (production.type === "gold") {
      return Math.ceil(val + (val * lvl * ratio));
    }
    return new Error("Unkown resource: " + production.type);
  }

  var evaluateNextLevelProduction = function evaluateNextLevelProduction(production, currLevel) {
    return evaluateProduction(production, currLevel + 1);
  }

  return {
    "evaluateNextLevelCost": evaluateNextLevelCost,
    "evaluateProduction": evaluateProduction,
    "evaluateNextLevelProduction": evaluateNextLevelProduction
  };
})();
