var logger = require('../../util/Logger')(console);

module.exports = (function() {

  var armyUtils = (function() {
    var mathUtils = require('../../util/math');

    function total(army, paramName) {
      var result = 0;
      for (var unitName in army) {
        if (army[unitName].hasOwnProperty(paramName)) {
          result += +army[unitName][paramName];
        } else {
          logger.log("ERROR", "[battle.js/army.total()] used with [paramName=" + paramName + "]. This object has no such property!")
        }
      }
      return result;
    };

    function isUnitAlive(unit) {
      return (unit.hp > 0);
    }

    function countUnitTypes(army) {
      var count = 0;
      for (var unitName in army) {
        if (isUnitAlive(army[unitName])) {
          ++count;
        }
      }
      return count;
    };

    function applyDamage(damageArray, army) {
      var index = 0;
      for (var unitName in army) {
        if (isUnitAlive(army[unitName])) {
          var effectiveDamage = damageArray[index] - army[unitName].def;
          if (effectiveDamage > 0) {
            army[unitName].hp -= effectiveDamage;
          }
          ++index;
        }
      }

      if (damageArray.length !== index) {
        logger.log("WARN", "[battle.js/applyDamage()] not all damage was handled [damageArray.length=" + damageArray.length + "; index=" + index + "]");
      }
    };

    function attack(sourceArmy, targetArmy) {
      var damageDivision = mathUtils.randomDivisionOfOne(countUnitTypes(targetArmy));
      var sourceArmyTotalAttack = total(sourceArmy, "att");
      var damage = damageDivision.map(dd => dd * sourceArmyTotalAttack);
      applyDamage(damage, targetArmy);
    };

    function fight(sourceArmy, targetArmy) {
      attack(sourceArmy, targetArmy);
      attack(targetArmy, sourceArmy);
    };

    function isArmyDead(army) {
      return (countUnitTypes(army) === 0);
    };

    var countUnitQuantity = function countUnitQuantity(unit, stats) {
      if (+unit.hp < 0) {
        unit.hp = 0;
      }
      return Math.ceil(+unit.hp / +stats.hp);
    }

    return {
      "fight": fight,
      "isArmyDead": isArmyDead,
      "countUnitQuantity": countUnitQuantity
    };
  })();

  var prepareArmy = function prepareArmy(armyInitialState, army) {
    if (!army) {
      army = {};
    }

    for (var unitName in armyInitialState) {
      var isInitial = false;
      if (!army[unitName]) {
        army[unitName] = {};
        isInitial = true;
      }

      var unitStats = armyInitialState[unitName].stats;
      army[unitName].quantity = (isInitial) ? +armyInitialState[unitName].quantity : armyUtils.countUnitQuantity(army[unitName], unitStats);

      army[unitName].hp = army[unitName].quantity * +unitStats.hp;
      army[unitName].att = army[unitName].quantity * +unitStats.att;
      army[unitName].def = army[unitName].quantity * +unitStats.def;
    };
    return army;
  };

  var prepareSingleArmyResultReport = function prepareSingleArmyResultReport(initArmyState, finalArmyState)  {
    var units = {};
    for (var uName in initArmyState) {
      units[uName] = {};
      units[uName].quantity = {};
      units[uName].quantity.before = +initArmyState[uName].quantity;
      units[uName].quantity.after = +finalArmyState[uName].quantity;
    }
    return units;
  };

  var prepareReport = function prepareReport(resultMsg, round, firstArmyInitialState, firstArmyFinalState, secondArmyInitialState, secondArmyFinalState) {
    var report = {
      result: resultMsg,
      round: round,
      user: {
        units: prepareSingleArmyResultReport(firstArmyInitialState, firstArmyFinalState)
      },
      adventure: {
        opponents: prepareSingleArmyResultReport(secondArmyInitialState, secondArmyFinalState)
      }
    };
    return report;
  };

  var processBattle = function processBattle(firstArmyInitialState, secondArmyInitialState) {
    var MAX_ROUNDS = 10;
    var report = {};
    var round = 0;

    // TODO usunac debug
    console.log("PROCESS BATTLE:")
    console.log("[first army]")
    console.log(firstArmyInitialState)
    console.log("[second army]")
    console.log(secondArmyInitialState)

    var battleResult = "";
    var firstArmy = null;
    var secondArmy = null;

    while (true) {
      ++round;

      firstArmy = prepareArmy(firstArmyInitialState, firstArmy);
      secondArmy = prepareArmy(secondArmyInitialState, secondArmy);

      console.log("[BATTLE] round=%d", round);
      console.log("[Before fight]");
      console.log(firstArmy);
      console.log(secondArmy);


      armyUtils.fight(firstArmy, secondArmy);

      console.log("[After fight]");
      console.log(firstArmy);
      console.log(secondArmy);


      if (armyUtils.isArmyDead(firstArmy)) {
        battleResult = "LOSE";
        break;
      } else if (armyUtils.isArmyDead(secondArmy)) {
        battleResult = "WIN";
        break;
      } else if (round >= MAX_ROUNDS) {
        battleResult = "DRAW";
        break;
      }
    }

    // recount after battle's finish 
    firstArmy = prepareArmy(firstArmyInitialState, firstArmy);
    secondArmy = prepareArmy(secondArmyInitialState, secondArmy);

    report = prepareReport(battleResult, round, firstArmyInitialState, firstArmy, secondArmyInitialState, secondArmy);

    return report;
  };

  return {
    "processBattle": processBattle  // TODO metoda do testow
  };
})();
