var logger = require('../../util/Logger')(console);
var arrayTools = require('../../util/arrays');
var mathTools = require('../../util/math');

module.exports = (function() {

  var getOpponentsGenerationPoints = function getOpponentsGenerationPoints(difficultyLevel) {
    var BASE_VALUE = 100;
    var MIN_VALUE = difficultyLevel * BASE_VALUE;
    var MAX_RATIO = 10;
    var MAX_VALUE = MIN_VALUE * MAX_RATIO;
    return Math.floor(Math.random() * (MAX_VALUE - MIN_VALUE)) + MIN_VALUE;
  };

  var prepareUnitsGenerationData = function prepareUnitsGenerationData(units) {
    for (var unitName in units) {
      var unit = units[unitName];
      unit.generation = {};

      var cost = +unit.cost.food + +unit.cost.gold;
      unit.generation.pointsCost = cost;
      if (cost !== 0) {
        unit.generation.probability = Math.sqrt(1 / cost);
      } else {
        logger.log("WARN", "Something went wrong during 'prepareUnitsGenerationData()'. Reason: Unit's generation points cost = 0 (which means that price of this unit is equal to 0)");
      }
    }
  };

  var __warTripAdventure_generateOpponents = function __warTripAdventure_generateOpponents(units, opponentsGenerationPoints) {
    var opponents = {};

    var unitsNames = Object.keys(units);
    arrayTools.shuffle(unitsNames);

    var unitsCosts = [];
    unitsNames.forEach( (name) => {
        unitsCosts.push(units[name].generation.pointsCost);
        opponents[name] = 0;  // init all opponents unit types
      } );

    var minCost = arrayTools.min(unitsCosts);
    delete unitsCosts;

    var missedGenerationActionLimit = Math.floor(opponentsGenerationPoints * 0.2);
    var missedGenerationAction = 0;

    logger.log("DEBUG", "[__warTripAdventure_generateOpponents()] Will start opponents generation. While loop condition params[opponentsGenerationPoints=" + opponentsGenerationPoints + "; minCost=" + minCost + "]")

    while (opponentsGenerationPoints > minCost) {
      var i, isGenerationMissed = true;
      for (i = 0; i < unitsNames.length; ++i) {
        var unitName = unitsNames[i];
        if (mathTools.draw(units[unitName].generation.probability) === mathTools.SUCCESSFUL_DRAW) {
          opponents[unitName] += 1;
          opponentsGenerationPoints = opponentsGenerationPoints - units[unitName].generation.pointsCost;
          isGenerationMissed = false;
          break;
        }
      }

      if (isGenerationMissed) {
        ++missedGenerationAction;
        if (missedGenerationAction >= missedGenerationActionLimit) {
          logger.log("WARN", "Adventure generation [generateWarTripAdventure()] finished prematurely. Reason: There was reached limit of missed generation actions [" + missedGenerationAction + "]");
          break;
        }
      }
    }

    logger.log("DEBUG", "Adventure generation [generateWarTripAdventure()] stats after finish: [missedGenerationAction=" + missedGenerationAction + "; missedGenerationActionLimit=" + missedGenerationActionLimit + "]")

    return opponents;
  };

  var __warTripAdventure_generateReward = function __warTripAdventure_generateReward(opponentsGenerationPoints) {
    var REWARD_FOOD_RATE = 2.5;
    var REWARD_GOLD_RATE = 1.5;

    return {
      food: Math.round(Math.random() * opponentsGenerationPoints * REWARD_FOOD_RATE),
      gold: Math.round(Math.random() * opponentsGenerationPoints * REWARD_GOLD_RATE)
    };
  }

  var generateWarTripAdventure = function generateWarTripAdventure(difficultyLevel) {
    var units = require('../../resources/eng/units').get();
    prepareUnitsGenerationData(units);

    var opponentsGenerationPoints = getOpponentsGenerationPoints(difficultyLevel);

    var adventure = {};
    adventure.name = "War Trip";
    adventure.details = {};
    adventure.details.opponents = __warTripAdventure_generateOpponents(units, opponentsGenerationPoints);
    adventure.details.reward = __warTripAdventure_generateReward(opponentsGenerationPoints);

    return adventure;
  };

  return {
    "generateWarTripAdventure": generateWarTripAdventure
  };
})();
