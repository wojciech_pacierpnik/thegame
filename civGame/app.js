var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');

var myLogger = require('./util/Logger')(console);

var mongoose = require('mongoose');
var dbConnection = require('./dao/connection');

var configuredPassport = require('./sign_in/passport_sign_in').createConfiguration(dbConnection).configuredPassport;

// Standard application's routes
var routes = require('./routes/index').createRoute(dbConnection);
var register = require('./routes/register').createRoute(dbConnection);
var login = require('./routes/login').createRoute(dbConnection);
var logout = require('./routes/logout').createRoute(dbConnection);
var account = require('./routes/account').createRoute(dbConnection);
var gameRouteTown = require('./routes/game/town').createRoute(dbConnection);
var gameRouteBarracks = require('./routes/game/barracks').createRoute(dbConnection);
var gameRouteAdventures = require('./routes/game/adventures').createRoute(dbConnection);
var highscoreRoutes = require('./routes/highscore').createRoute(dbConnection);

// Angular Single Page Application's routes
var gameSpaRoute = require('./routes/indexSpa').createRoute(dbConnection);
var gameRouteLoginRestApi = require('./routes/rest_api/login').createRoute(dbConnection);
var gameRouteTownRestApi = require('./routes/rest_api/town').createRoute(dbConnection);
var gameRouteBarracksRestApi = require('./routes/rest_api/barracks').createRoute(dbConnection);
var gameRouteAdventuresRestApi = require('./routes/rest_api/adventures').createRoute(dbConnection);
var gameRouteHighscoreRestApi = require('./routes/rest_api/highscore').createRoute(dbConnection);

// helper values
var hour = 3600000;

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
  secret: 'My secret secret',
  resave: false,
  saveUninitialized: false,
  cookie: { secure: false, maxAge: hour }
}));
app.use(configuredPassport.initialize());
app.use(configuredPassport.session());

app.use('/', routes);
app.use('/registration', register);
app.use('/login', login);
app.use('/logout', logout);
app.use('/account', account);
app.use('/game/town', gameRouteTown);
app.use('/game/barracks', gameRouteBarracks);
app.use('/game/adventures', gameRouteAdventures);
app.use('/game/highscore', highscoreRoutes);

// SPA - Single Page App
app.use('/spa/game', gameSpaRoute);
app.use('/api/io/', gameRouteLoginRestApi);
app.use('/api/game/town', gameRouteTownRestApi);
app.use('/api/game/barracks', gameRouteBarracksRestApi);
app.use('/api/game/adventures', gameRouteAdventuresRestApi);
app.use('/api/game/highscore', gameRouteHighscoreRestApi);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

process.on('exit', function() {
  mongoose.connection.close(function () {
    myLogger.log('Mongoose disconnected on app termination');
    process.exit(0);
  });
});

process.on('SIGINT', function() {
  mongoose.connection.close(function () {
    myLogger.log('Mongoose disconnected on app SIGINT signal');
    process.exit(0);
  });
});

process.on('uncaughtException', function(err) {
  console.log(err);
  mongoose.connection.close(function () {
    myLogger.log('Mongoose disconnected on app uncaught exception');
    process.exit(0);
  });
});

module.exports = app;
