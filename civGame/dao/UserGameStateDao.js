exports.createDao = function createDao(dbConn) {
  var UserGameState = dbConn.UserGameState;
  //var ObjectId = require('mongoose').Types.ObjectId;

  var playerUtils = require('../game/util/player');
  var highscoreUtils = require('../game/util/highscore');
  var logger = require('../util/Logger')(console);

  var TO_HOURS = (1 / (1000 * 60 * 60));

  return (function() {
    var updateUserGameStateOnAccess = function updateUserGameStateOnAccess(userGameState) {
      var buildings = playerUtils.getBuildingsState(userGameState);
      var sinceLastModified = (+Date.now() - +userGameState.lastModified) * TO_HOURS; // hours with fractional part

      for (var key in buildings) {
        var production = buildings[key].production;
        if (production) {
          userGameState.resources[production.type] =
            +userGameState.resources[production.type] + (+production.currentValue * sinceLastModified);
        }
      }

      userGameState.highscore = highscoreUtils.calculateHighscore(userGameState);

      userGameState.lastModified = Date.now();
      userGameState.save();
    };

    var getUserGameState = function getUserGameState(id, callback) {
      UserGameState.find({refUser: id}, function(err, gameState) {
        if (err) {
          return callback(err, null);
        }

        if (gameState.length === 0) {
          return initUserGameState(id, function(initErr, newGameState) {
            updateUserGameStateOnAccess(newGameState);
            callback(initErr, newGameState);
          });
        }
        var resultGameState = (gameState != null) ? gameState[0] : null;
        updateUserGameStateOnAccess(resultGameState);
        callback(err, resultGameState);
      });
    };

    var initUserGameState = function initUserGameState(id, callback) {
      var ugs = new UserGameState({
        refUser: id,
        resources: {
          food: 1000,
          gold: 500
        },
        buildings: {},
        units: {},
        highscore: 0
      });

      ugs.markModified('buildings');  // TODO [raczej do usuniecia] sprawdzic czy to jest konieczne w tym wypadku (tworzenie nowego obiektu, a nie modyfikacja istniejacego)
      ugs.save(callback);
    };

    /*var updateUserGameState = function updateUserGameState(id, newState, callback) {
      if (Object.keys(newState).length === 0) {
        return;
      }

      UserGameState.findOne({_id: id}, function(err, userGameState) {
        for (var key in newState) {
          // TODO napisac logike
        }
      });
    };*/

    var removeUserGameState = function removeUserGameState(id, callback) {
      UserGameState.deleteOne({ refUser: id }, callback);
    };

    return {
      "getUserGameState": getUserGameState,
      "initUserGameState": initUserGameState//,
      //"updateUserGameState": updateUserGameState
      ,"removeUserGameState": removeUserGameState
    };
  })();
};
