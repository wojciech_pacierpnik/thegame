var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var logger = require('../util/Logger')(console);

var URL = require('../resources/config/conf.json').database.url;
mongoose.connect(URL);
var db = mongoose.connection;

db.on('error', function(err) {
  logger.log(err);
});

db.once('open', function() {
  logger.log("[Database] Connection correctly opened!");
});

var userSchema = new Schema({
    username: { type: String, unique: true, required: true },
    password: { type: String, required: true },
    email: { type: String, unique: true, required: true },
    admin: { type: Boolean, default: false },
    active: { type: Boolean, default: false },
    blocked: { type: Boolean, default: false }
  });

var userGameStateSchema = new Schema({
    refUser: { type: Schema.Types.ObjectId, required: true },
    lastModified: { type: Date, default: Date.now },
    resources: {
      food: { type: Number },
      gold: { type: Number }
    },
    buildings: {},
    units: {},
    highscore: { type: Number, default: 0 }
  },
  { // schema options
    minimize: false   // will save empty objects
  });

var gameEventSchema = new Schema({
    refInitUser: { type: Schema.Types.ObjectId, required: true },
    initDate: { type: Date, required: true },
    finishDate: { type: Date, required: true },
    description: { type: String },
    additionalData: {}
  });

var adventureSchema = new Schema({
    refUser: { type: Schema.Types.ObjectId, required: true },
    name: { type: String, required: true },
    initDate: { type: Date, default: Date.now },
    endDate: { type: Date },
    details: {}
  },
  { // schema options
    minimize: false   // will save empty objects
  });

var jwtTokenSchema = new Schema({
    refUser: { type: Schema.Types.ObjectId, required: true },
    token: { type: String, required: true },
    initDate: { type: Date, default: Date.now },
    expDate: { type: Date },
    isValid: { type: Boolean, default: true }
  });

module.exports = {
  User: mongoose.model('User', userSchema),
  UserGameState: mongoose.model('UserGameState', userGameStateSchema),
  GameEvent: mongoose.model('GameEvent', gameEventSchema),
  Adventure: mongoose.model('Adventure', adventureSchema),
  JwtToken: mongoose.model('JwtToken', jwtTokenSchema)
};
