exports.createDao = function createDao(dbConn) {
  var UserGameState = dbConn.UserGameState;
  var User = dbConn.User;
  var logger = require('../util/Logger')(console);
  var highscoreUtils = require('../game/util/highscore');

  return (function() {

    /**
      @fromPos position from which (including) highscore results will be returned
      @limit amount of result to return
    */
    var fetchHighscore = function fetchHighscore(fromPos, limit, callback) {
      UserGameState.find({}) //find all
        .skip(fromPos) //from position
        .limit(limit) //(??)number of docs to fetch
        .sort({ highscore: -1 }) //sort DESC by highscore
        .select({'refUser': 1, 'highscore': 1}) //fetch only fields
        .exec(function(err, docs) {
            if (err) {
              logger.log("ERROR", err);
              return next(err);
            }

            var refUserIds = docs.map(obj => obj["refUser"]);

            User.find({
                _id: { $in: refUserIds }
              })
              .select({ _id: 1, username: 1 })
              .exec(function(userFindErr, usersFoundDocs) {
                if (userFindErr) {
                  logger.log("ERROR", userFindErr);
                  return next(userFindErr);
                }

                var mergedUsers = highscoreUtils.mergeUserGameStatesWithUsernames(docs, usersFoundDocs);

                callback(userFindErr, mergedUsers);
              });
          });
    };

    return {
      "fetchHighscore": fetchHighscore
    };
  })();
};
