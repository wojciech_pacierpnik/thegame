exports.createDao = function createDao(dbConn) {
  var Adventure = dbConn.Adventure;
  //var ObjectId = require('mongoose').Types.ObjectId;

  //var playerUtils = require('../game/util/player');
  var logger = require('../util/Logger')(console);
  var adventureUtils = require('../game/util/adventure');

  //var TO_HOURS = (1 / (1000 * 60 * 60));

  /// TODO modul do dokonczenia oraz testow

  return (function() {
    var ADVENTURES_NUMBER = 5;

    var generateAdventures = function generateAdventures(userId, number, callback) {
      var _ADVENTURE_DIFFICULTY_LEVEL_ = Math.round(Math.random() * 20);

      var adventures = [];
      for (var i = 0; i < number; ++i) {
        var adventure = adventureUtils.generateWarTripAdventure(_ADVENTURE_DIFFICULTY_LEVEL_);
        adventure.refUser = userId;
        adventures.push(adventure);
      }
      Adventure.insertMany(adventures, callback);
    };

    var getAdventures = function getAdventures(userId, callback) {
      Adventure.find({refUser: userId}, function(err, adventures) {
        if (err) {
          return callback(err);
        }

        if (adventures.length < ADVENTURES_NUMBER) {
          logger.log("DEBUG", "Will generate " + (ADVENTURES_NUMBER - adventures.length) + " adventures");
          return generateAdventures(userId, (ADVENTURES_NUMBER - adventures.length), callback);
        }

        callback(null, adventures);
      });
    };

    var removeAdventure = function removeAdventure(adventureId, callback) {
      Adventure.deleteOne({_id: adventureId}, callback);
    };

    var getAdventure = function getAdventure(adventureId, callback) {
      Adventure.findOne({_id: adventureId}, callback);
    };

    return {
      "getAdventures": getAdventures,
      "generateAdventures": generateAdventures,
      "removeAdventure": removeAdventure,
      "getAdventure": getAdventure
    };
  })();
};
