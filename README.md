# README #

This repository contains project for university's course.

To run project you need:

* npm in version at least 4.1.2
* node in version at least 7.10.0
* mongoDB in version at least 2.6.10
* (optionally) nodemon (v. 1.11.0) - if not using nodemon, there is package.json start script to adjust

Above requirements describes development environment and the conditions where the app' was tested.

To run project perform:

* cd /path/to/project
* npm install
* npm start